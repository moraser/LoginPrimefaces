/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author ing. Frank Morales
 */
@Named(value = "loginControler")
@ViewScoped
public class LoginControler implements Serializable {
    
    @PostConstruct
    public void init(){
        usuario = "";
        password = "";
    }

    private String usuario;
    private String password;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String validaLogin(){
        if("admin".equals(usuario) && "admin".equals(password)){
            return "principal.jsf?faces-redirect=true";
        }else{
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "ERROR DE VALIDACION", "USUARIO O CONTRASEÑA INCORRECTOS");
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
            return null;
        }
    }
    
}
